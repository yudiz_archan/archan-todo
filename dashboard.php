<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
    <link rel="stylesheet" href="./css/dashboard.css">
    <title>Login</title>
</head>

<body onload="getWeather(); getGifs();">
    <section id="header">
        <h2>Dashboard</h2>
        <div>
            <ul id="navbar">
                <li><a class="active" href="./profile.html">
                        <div class="profile-div">
                            <img src="./assets/Profile-Male-PNG.png" width="30px" height="30px">
                        </div>
                    </a></li>
                <li><a href="./index.php">Logout</a></li>
                <a href="#" id="close"><i class="fa fa-window-close"></i></a>
            </ul>
        </div>
        <div id="mobile">
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>
    <div class="mainContainer">
        <div class="taskContainer">
            <div id="head">
                <input type="text" id="taskDetails" placeholder="Your Task">
                <button id="addTask" onclick="addTask()">Add Task</button>
            </div>
            <div id="taskScreen">
                <div id="pending">
                    <h3>Pending Task</h3>
                </div>
                <div id="completed">
                    <h3>Completed Task</h3>
                </div>
            </div>
        </div>
        <div class="thirdpartyContainer">
            <div id="timer"></div>
            <div id="weather">
                <p id="temp"></p>
                <p id="desc"></p>
                <p id="loc"></p>
            </div>
            <div id="gif">
                <img id="gifs" src="" alt="">
            </div>
        </div>
    </div>
    <script src="./js/dashboard.js"></script>
</body>

</html>