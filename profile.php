<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
    <link rel="stylesheet" href="./css/profile.css">
    <title>Profile</title>
</head>

<body>
    <section id="header">
        <h3>Profile</h3>
        <div>
            <ul id="navbar">
                <li><a href="./changePassword.php">Chnage Password</a></li>
                <li><a href="./index.php">Logout</a></li>
                <a href="#" id="close"><i class="fa fa-window-close"></i></a>
            </ul>
        </div>
        <div id="mobile">
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>
    <div class="container">
        <div class="detail-container">
            <h3>Details</h3>
            <span>Fullname: <input id="fullName" type="text" disabled></span>
            <span>Username: <input id="userName" type="text" disabled></span>
            <span>Email ID: <input id="emailId" type="text" disabled></span>
            <span>Phone Number: <input id="phoneNumber" type="text" disabled></span>
        </div>
        <div class="buttons">
            <input type="submit" name="Edit" id="edit" value="Edit">
            <input type="submit" name="Save" id="save" value="Save">
        </div>
    </div>
    <script src="./js/profile.js"></script>
</body>

</html>