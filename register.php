<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/styles.css">
    <title>Registration</title>
</head>

<body>
    <div class="container">
        <div class="title">Registration</div>
        <form action="#" onsubmit="submitData()">
            <div class="user-details">
                <div class="input-box">
                    <span class="details">Full Name</span>
                    <input type="text" id="fullName" placeholder="Enter your name" required />
                </div>
                <div class="input-box">
                    <span class="details">Username</span>
                    <input type="text" id="userName" placeholder="Enter your username" required />
                </div>
                <div class="input-box">
                    <span class="details">Email</span>
                    <input type="email" id="emailId" placeholder="Enter your email id" required />
                </div>
                <div class="input-box">
                    <span class="details">Phone Number</span>
                    <input type="tel" id="phoneNumber" placeholder="Enter your number" required maxlength="10"
                        minlength="10" />
                </div>
                <div class="input-box">
                    <span class="details">Password</span>
                    <input type="password" id="passWord" placeholder="Enter your password" required />
                </div>
                <div class="input-box">
                    <span class="details">Confirm Password</span>
                    <input type="password" id="confirmPassword" placeholder="Confirm password" required />
                </div>
            </div>
            <div class="gender-details">
                <input type="radio" name="gender" id="dot-1" value="Male" />
                <input type="radio" name="gender" id="dot-2" value="Female" />
                <input type="radio" name="gender" id="dot-3" value="Prefer not to say" checked="checked" />
                <span class="gender-title">Gender</span>
                <div class="category">
                    <label for="dot-1">
                        <span class="dot one"></span>
                        <span class="gender">Male</span>
                    </label>
                    <label for="dot-2">
                        <span class="dot two"></span>
                        <span class="gender">Female</span>
                    </label>
                    <label for="dot-3">
                        <span class="dot three"></span>
                        <span class="gender">Prefer not to say</span>
                    </label>
                </div>
                <div class="container-2">
                    <div class="birthdate">
                        <span class="birthdate-title">Birthdate</span>
                        <div class="date-box">
                            <input type="date" name="Date" id="dates" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="button">
                <input id="submit" type="submit" value="Register"></input>
            </div>
            <div class="linkPage">
                <span>Already have account? </span>
                <a href="./index.php">Login here</a>
            </div>
        </form>
    </div>
    <script src="./js/script.js"></script>
</body>

</html>