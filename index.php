<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/styles.css">
    <title>Login</title>
</head>

<body>
    <div class="container">
        <div class="title">Login</div>
        <form action="./dashboard.php" onsubmit="login()">
            <div class="user-details">

                <div class="input-box">
                    <span class="details">Email</span>
                    <input type="email" id="emailId" placeholder="Enter your email id" required />
                </div>
                <div class="input-box">
                    <span class="details">Password</span>
                    <input type="password" id="passWord" placeholder="Enter your password" required />
                </div>
            </div>

            <div class="button">
                <input id="submit" type="submit" value="Login"></input>
            </div>
            <div class="linkPage">
                <span>Not Registered? </span>
                <a href="./register.php">Register here</a>
            </div>
        </form>
    </div>
    <script src="./js/script.js"></script>
</body>

</html>