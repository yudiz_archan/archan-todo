const bar = document.getElementById('bar');
const close = document.getElementById('close');
const nav = document.getElementById('navbar');
let timer = document.getElementById('timer')
let taskScreen = document.getElementById('taskScreen');
let pendingTask = document.getElementById('pending');
let completedTask = document.getElementById('completed');
let taskDetails = document.getElementById('taskDetails');
let sTaskInput = "task";
let nTaskCount = 0;
let oTasks = [];

if (bar) {
    bar.addEventListener('click', () => {
        nav.classList.add('active')
    })
}

if (close) {
    close.addEventListener('click', () => {
        nav.classList.remove('active')
    })
}

function refreshTime() {
    const timeDisplay = document.getElementById("timer");
    const timeString = new Date().toLocaleTimeString();
    timeDisplay.textContent = timeString;
}
setInterval(refreshTime, 1000);


function addTask() {
    if (taskDetails.value === "") {
        alert("Enter details");
    } else {
        nTaskCount++;
        var nDynamicDiv = document.createElement('div');
        nDynamicDiv.style = "margin:10px 0; width:100%; height:auto; display:flex; flex-direction:row; flex-wrap:nowrap; justify-content:space-between; align-item:center;"

        var checkbox = document.createElement('input');
        checkbox.type = "checkbox";
        checkbox.name = "name";
        checkbox.value = "value";
        checkbox.id = `checkBox_${nTaskCount}`;
        checkbox.style = "margin:10px"

        var label = document.createElement('label')
        label.htmlFor = "id";

        nDynamicDiv.appendChild(checkbox);
        nDynamicDiv.appendChild(label);
        pendingTask.appendChild(nDynamicDiv);

        // console.log("Inner");
        var nDynamicInput = document.createElement('input');
        nDynamicInput.type = 'text';
        nDynamicInput.placeholder = "Your Task";
        nDynamicInput.value = taskDetails.value;
        nDynamicInput.id = `${sTaskInput}_${nTaskCount}`;
        nDynamicInput.disabled = true
        nDynamicInput.style = "width:100%; border: 3px solid #40916c; margin:5px; padding: 10px; border-radius: 5px; "
        nDynamicDiv.appendChild(nDynamicInput);
        pendingTask.appendChild(nDynamicDiv);

        var nDynamicEdit = document.createElement('button');
        nDynamicEdit.innerHTML = "Edit";
        nDynamicEdit.style = "width: 60px; color: #fff; background: #40916c; border:none; border-radius: 5px; margin: 5px"
        nDynamicDiv.appendChild(nDynamicEdit);
        pendingTask.appendChild(nDynamicDiv);

        var nDynamicDelete = document.createElement('button');
        nDynamicDelete.innerHTML = "Delete";
        nDynamicDelete.style = "width: 80px; color: #fff; background: #b3382f; border:none; border-radius:5px; margin: 5px"
        nDynamicDiv.appendChild(nDynamicDelete);
        pendingTask.appendChild(nDynamicDiv);

        let task = {
            taskId: nDynamicInput.id,
            taskValue: taskDetails.value,
            taskPending: true
        }
        oTasks.push(task);
        let userData = JSON.parse(localStorage.getItem('userData'));
        let index = sessionStorage.getItem('index');
        userData[index].tasks = oTasks;
        localStorage.setItem('userData', JSON.stringify(userData))
        console.log(oTasks);
    }
}



function getWeather() {
    const temperature = document.getElementById("temp");
    const description = document.getElementById("desc");
    const location = document.getElementById("loc");

    const api = "https://api.openweathermap.org/data/2.5/weather";
    const apiKey = "a4f34114df09ff8466f17741eea80936";
    location.innerHTML = "Locating...";
    navigator.geolocation.getCurrentPosition(success, error);
    async function success(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        const url = api + "?lat=" + latitude + "&lon=" + longitude + "&appid=" + apiKey + "&units=imperial";
        const response = await fetch(url);
        const json = await response.json();
        temperature.innerHTML = Math.round(json.main.temp) + "° F";
        location.innerHTML = json.name;
        description.innerHTML = json.weather[0].main;
    }
    function error() {
        location.innerHTML = "Unable to retrieve your location";
    }
}
let gifIndex = 0;
async function getGifs() {
    const gifUrl = 'https://api.giphy.com/v1/gifs/search?q=mrbean&api_key=Poco4yPP44g5bmUoNJ4HVPkDnARgQI0e';
    const gifResponse = await fetch(gifUrl);
    const gifJSON = await gifResponse.json();
    document.getElementById('gifs').src = gifJSON.data[gifIndex].images.original.url;
    gifIndex++;
}

setInterval(getGifs, 120000)
