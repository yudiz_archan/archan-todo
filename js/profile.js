const bar = document.getElementById('bar');
const close = document.getElementById('close');
const nav = document.getElementById('navbar');

if (bar) {
    bar.addEventListener('click', () => {
        nav.classList.add('active')
    })
}

if (close) {
    close.addEventListener('click', () => {
        nav.classList.remove('active')
    })
}

let user = JSON.parse(sessionStorage.getItem('user'));
console.log(user);
document.getElementById("fullName").value = user.fullName;
document.getElementById("userName").value = user.userName;
document.getElementById("emailId").value = user.emailId;
document.getElementById("phoneNumber").value = user.phoneNumber;

let editData = document.getElementById('edit');
editData.addEventListener('click', function () {
    console.log("In");
    document.getElementById("fullName").disabled = false;
    document.getElementById("userName").disabled = false;
    document.getElementById("emailId").disabled = false;
    document.getElementById("phoneNumber").disabled = false;
})


let saveData = document.getElementById('save');
saveData.addEventListener('click', function () {
    console.log("In");
    document.getElementById("fullName").disabled = true;
    document.getElementById("userName").disabled = true;
    document.getElementById("emailId").disabled = true;
    document.getElementById("phoneNumber").disabled = true;
    let fullName = document.getElementById("fullName").value;
    let userName = document.getElementById("userName").value;
    let emailId = document.getElementById("emailId").value;
    let phoneNumber = document.getElementById("phoneNumber").value;


    let getData = JSON.parse(localStorage.getItem('userData'));

    for (let i = 0; i < getData.length; i++) {
        if (emailId === getData[i].emailId) {
            console.log("Email Success");
            getData[i].fullName = fullName;
            getData[i].userName = userName;
            getData[i].emailId = emailId;
            getData[i].phoneNumber = phoneNumber;

            localStorage.setItem('userData', JSON.stringify(getData))
            break;
        }
    }

})