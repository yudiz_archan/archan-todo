const letters = /^[A-Za-z]+$/;
const userRegex = /^[0-9a-zA-Z]+$/;
const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const mobileFormat = /^[6789]\d{9}$/;
let allUsers = [];

function submitData() {
    let fullName = document.getElementById('fullName').value;
    let userName = document.getElementById('userName').value;
    let emailId = document.getElementById('emailId').value;
    let phoneNumber = document.getElementById('phoneNumber').value;
    let password = document.getElementById('passWord').value;
    let confirmPassword = document.getElementById('confirmPassword').value;
    var radios = document.getElementsByName("gender");
    var gender = Array.from(radios).find(radio => radio.checked).value;


    if (!fullName.match(letters)) {
        alert("Full name contains only alphabets")
    } else if (!userName.match(userRegex)) {
        alert("User name contains only alphabets and numbers")
    } else if (!emailId.match(mailformat)) {
        alert("Please enter valid email address")
    } else if (!phoneNumber.match(mobileFormat)) {
        alert("Please enter valid phone number")
    } else if (confirmPassword !== password) {
        alert("Password and Confirm password should match")
    } else {
        let user = {
            fullName: fullName,
            userName: userName,
            emailId: emailId,
            phoneNumber: phoneNumber,
            gender: gender,
            password: password,
            tasks: null
        }

        let getData = JSON.parse(localStorage.getItem('userData'));

        if (getData === null) {
            allUsers.push(user)
            localStorage.setItem('userData', JSON.stringify(allUsers))
        } else {
            getData.push(user)
            localStorage.setItem('userData', JSON.stringify(getData))
        }

    }


    // console.log(allUsers[0])
    // allUsers = JSON.parse(localStorage.getItem('userData'));
    // console.log(allUsers)
}

function login() {
    let getData = JSON.parse(localStorage.getItem('userData'));
    let emailId = document.getElementById('emailId').value;
    let password = document.getElementById('passWord').value;
    let aErrorMessage = [];
    let nCount = 0;
    console.log(getData);

    for (let i = 0; i < getData.length; i++) {
        if (emailId === getData[i].emailId) {
            console.log("Email Success");
            if (password === getData[i].password) {
                console.log("Password Success");
                sessionStorage.setItem('user', JSON.stringify(getData[i]))
                sessionStorage.setItem('index', JSON.stringify(i))
                return true
                // window.location.assign("./dashboard.php");
                // alert("Success")
                break
            } else {
                console.log("Incorrect");
                aErrorMessage.push("Incorrect Username and Password")

            }
        } else {
            nCount++;
        }
    }
    if (aErrorMessage.length != 0) {
        alert(aErrorMessage[0])
    } else if (nCount != 0) {
        alert("Username doesn't exist")
    }
}

