<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
    <link rel="stylesheet" href="./css/changePassword.css">
    <title>Profile</title>
</head>

<body>
    <section id="header">
        <h3>Change Password</h3>
        <div>
            <ul id="navbar">
                <li><a href="./index.php">Logout</a></li>
                <a href="#" id="close"><i class="fa fa-window-close"></i></a>
            </ul>
        </div>
        <div id="mobile">
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>
    <div class="container">
        <div class="detail-container">
            <h3>Change Password</h3>
            <span>Old Password: <input id="oldPassword" type="text"></span>
            <span>New Password: <input id="newPassword" type="text"></span>
            <span>Confirm Password: <input id="confirmPassword" type="text"></span>
        </div>
        <div class="buttons">
            <input type="submit" name="Save" id="save" value="Save">
        </div>
    </div>
    <script src="./js/confirmPassword.js"></script>
</body>

</html>